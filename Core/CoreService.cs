﻿using Core.Inputs;
using Core.Routing;

namespace Core
{
    public interface ICoreService
    {
        /// <summary>
        /// Compute the probability that Millenium Falcon reaches its destination in time
        /// </summary>
        double GetSuccessProbability(MilleniumFalconInput milleniumFalconInput, EmpireInput empireInput);
    }

    public class CoreService: ICoreService
    {
        private readonly RoutePlanner _routePlanner;
        private readonly ProbabilityComputer _probabilityComputer;
        public CoreService(RoutePlanner routePlanner, ProbabilityComputer probabibilityComputer )
        {
            _routePlanner = routePlanner;
            _probabilityComputer = probabibilityComputer;
        }

        public double GetSuccessProbability(MilleniumFalconInput milleniumFalconInput, EmpireInput empireInput)
        {
            var routes = _routePlanner.ComputeValidRoutes(milleniumFalconInput.Departure, milleniumFalconInput.Arrival, milleniumFalconInput.Autonomy, empireInput.Countdown);
            var bountyHunterSchedule = new BountyHunterSchedule(empireInput.BountyHunterLocations);
            var successProbability = _probabilityComputer.ComputeHighestProbabilityOfSuccess(routes, bountyHunterSchedule, empireInput.Countdown);
            return successProbability;
        }
    }
}
