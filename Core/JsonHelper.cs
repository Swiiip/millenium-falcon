using Newtonsoft.Json;

namespace Core
{
    public static class JsonHelper {
        public static bool TryDeserialize<T>(string jsonString, out T val) {
            try
            {
                val =  JsonConvert.DeserializeObject<T>(jsonString);
                return true;
            }
            catch (Newtonsoft.Json.JsonReaderException)
            {
                val = default;
                return false;
            }
        }
    }
}