using Newtonsoft.Json;

namespace Core.Inputs
{
    public class MilleniumFalconInput
    {
        [JsonProperty("autonomy")]
        public int Autonomy { get; set; }

        [JsonProperty("departure")]
        public string Departure { get; set; }

        [JsonProperty("arrival")]
        public string Arrival { get; set; }

        [JsonProperty("routes_db")]
        public string DbPath { get; set; }
    }
}