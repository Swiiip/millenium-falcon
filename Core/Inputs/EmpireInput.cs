using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Inputs
{
    public class EmpireInput
    {
        [JsonProperty("countdown")]
        public int Countdown { get; set; }

        [JsonProperty("bounty_hunters")]
        public List<BountyHunterLocation> BountyHunterLocations { get; set; }
    }

    public class BountyHunterLocation
    {
        [JsonProperty("planet")]
        public string Planet { get; set; }

        [JsonProperty("day")]
        public int Day { get; set; }
    }
}