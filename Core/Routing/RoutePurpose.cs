namespace Core.Routing
{
    public enum RoutePurpose
    {
        Travel = 0,
        Refuel = 1,
    }
}
