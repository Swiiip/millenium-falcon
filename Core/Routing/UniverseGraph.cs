using Core.Data;
using System.Collections.Generic;

namespace Core.Routing
{
    public class UniverseGraph
    {
        private readonly Dictionary<string, Dictionary<string, int>> _graph;

        public UniverseGraph(ITravelInfoReader reader)
        {

            _graph = new Dictionary<string, Dictionary<string, int>>();

            var travelInfos = reader.ReadAllTravelInfo();

            foreach (var entry in travelInfos)
            {
                _graph[entry.Origin] = _graph.GetValueOrDefault(entry.Origin, new Dictionary<string, int>());
                _graph[entry.Destination] = _graph.GetValueOrDefault(entry.Destination, new Dictionary<string, int>());

                _graph[entry.Origin][entry.Destination] = entry.TravelTime;
                _graph[entry.Destination][entry.Origin] = entry.TravelTime;
            }
        }

        public Dictionary<string, int> GetNeighboringPlanets(string planetName)
        {
            return _graph.GetValueOrDefault(planetName, new Dictionary<string, int>());
        }
    }
}