using System.Collections.Generic;

namespace Core.Routing
{
    public interface IRoutePlanner {

        /* Compute all possible routes between origin and destination under autonomy and countdown constraints */
        List<Route> ComputeValidRoutes(string origin, string destination, int autonomy, int countdown);
    }

    public class RoutePlanner: IRoutePlanner
    {
        private readonly UniverseGraph _universeGraph;

        public RoutePlanner(UniverseGraph universeGraph)
        {
            _universeGraph = universeGraph;
        }

        public List<Route> ComputeValidRoutes(string origin, string destination, int autonomy, int countdown)
        {
            var result = new List<Route>();
            var currentRoute = new Route();
            var visitedPlanets = new HashSet<string>();

            DFS(origin, destination, autonomy, autonomy, countdown, visitedPlanets, currentRoute, result);
            return result;
        }

        private void DFS(
            string currentPlanet,
            string destination,
            int autonomy,
            int remainingFuel,
            int remainingTime,
            HashSet<string> visitedPlanets,
            Route currentRoute,
            List<Route> validRoutes
        )
        {

            // Stop conditions
            if (visitedPlanets.Contains(currentPlanet) || remainingTime < 0 || remainingFuel < 0)
                return;

            currentRoute.Steps.AddLast(new RouteStep(currentPlanet, currentRoute.TotalTravelTime, RoutePurpose.Travel));

            if (currentPlanet == destination)
            {
                // The current route is valid
                validRoutes.Add(new Route(currentRoute));
            }
            else
            {
                // Mark the current planet visited
                visitedPlanets.Add(currentPlanet);

                // Iterate through neighboring planets and recursively compute available routes
                var neighbors = _universeGraph.GetNeighboringPlanets(currentPlanet);

                foreach (var entry in neighbors)
                {
                    var nextPlanet = entry.Key;
                    var distance = entry.Value;

                    var shouldRefuel = remainingFuel < distance;
                    var remFuel = shouldRefuel ? autonomy : remainingFuel;

                    // Refueling takes 1 more day
                    distance = shouldRefuel ? distance + 1 : distance;

                    // Update the current route state
                    if (shouldRefuel)
                        currentRoute.Steps.AddLast(new RouteStep(currentPlanet, currentRoute.TotalTravelTime + 1, RoutePurpose.Refuel));
                    currentRoute.TotalTravelTime += distance;

                    // Recursively compute the next possible routes
                    DFS(nextPlanet, destination, autonomy, remFuel - distance, remainingTime - distance, visitedPlanets, currentRoute, validRoutes);

                    // Reset the current route state
                    currentRoute.TotalTravelTime -= distance;
                    if (shouldRefuel)
                        currentRoute.Steps.RemoveLast();
                }

                // Reset the visited state of the current planet
                visitedPlanets.Remove(currentPlanet);
            }

            currentRoute.Steps.RemoveLast();
            return;
        }
    }
}
