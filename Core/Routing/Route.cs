using System.Collections.Generic;

namespace Core.Routing
{
    public class Route
    {
        public LinkedList<RouteStep> Steps;

        public int TotalTravelTime;

        public Route() {
            Steps = new LinkedList<RouteStep>();
            TotalTravelTime = 0;
        }

        public Route(Route route) {
            Steps = new LinkedList<RouteStep>(route.Steps);
            TotalTravelTime = route.TotalTravelTime;
        }
    }
}
