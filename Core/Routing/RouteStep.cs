namespace Core.Routing
{
    public struct RouteStep
    {
        public string Planet;
        public int Day;
        public RoutePurpose Purpose;
        public RouteStep(string planet, int day, RoutePurpose purpose)
        {
            Planet = planet;
            Day = day;
            Purpose = purpose;
        }
    }
}
