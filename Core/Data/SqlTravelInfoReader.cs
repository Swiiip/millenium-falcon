using Core.Data.Models;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Core.Data
{
    public class SqlTravelInfoReader : ITravelInfoReader
    {

        private readonly SqlContext _sqlContext;
        private readonly ILogger _logger;

        public SqlTravelInfoReader(SqlContext sqlContext, ILogger logger) {
            _sqlContext = sqlContext;
            _logger = logger;
        }

        public List<TravelInfo> ReadAllTravelInfo() {
            try
            {
                using (_sqlContext)
                {
                    var travelInfos = _sqlContext.TravelInfos.ToList();
                    return travelInfos;
                }
            }
            catch (System.Exception e)
            {
                _logger.LogError("Error reading travel info from sql context", e);
                return new List<TravelInfo>();
            }
        }
    }
}
