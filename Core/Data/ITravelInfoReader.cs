using Core.Data.Models;
using System.Collections.Generic;

namespace Core.Data
{
    public interface ITravelInfoReader {
        /// <summary>
        /// Read TravelInfo data from multiple sources (Sql, Json for testing, etc.)
        /// </summary>
        public List<TravelInfo> ReadAllTravelInfo();
    }
}
