using Core.Data.Models;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;

namespace Core.Data
{
    public class JsonTravelInfoReader : ITravelInfoReader
    {

        private readonly string _path;
        private readonly ILogger _logger;

        public JsonTravelInfoReader(string path, ILogger logger) {
            _path = path;
            _logger = logger;
        }

        public List<TravelInfo> ReadAllTravelInfo() {
            try
            {
                var travelInfoStr = File.ReadAllText(_path);
                var parseSuccess = JsonHelper.TryDeserialize<List<TravelInfo>>(travelInfoStr, out var travelInfos);
                if (parseSuccess) {
                    return travelInfos;
                }
                _logger.LogError("Could not parse travel info from json");
            }
            catch (System.Exception e)
            {
                _logger.LogError("Error reading travel info from json", e);
            }
            return new List<TravelInfo>();
        }
    }
}
