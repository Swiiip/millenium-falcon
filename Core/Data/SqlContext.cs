using Microsoft.EntityFrameworkCore;

namespace Core.Data
{
    public class SqlContext : DbContext
    {
        public DbSet<Models.TravelInfo> TravelInfos { get; set; }

        private string _dbPath;

        public SqlContext(string path) {
            _dbPath = path;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite($"Data Source={_dbPath};Mode=ReadOnly");

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Models.TravelInfo>().HasKey(c => new { c.Origin, c.Destination });
            modelBuilder.Entity<Models.TravelInfo>().ToTable("routes");
        }
    }
}
