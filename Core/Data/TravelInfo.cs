using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Data.Models
{
    public class TravelInfo
    {
        [Column("origin")]
        public string Origin { get; set; }

        [Column("destination")]
        public string Destination { get; set; }

        [Column("travel_time")]
        public int TravelTime { get; set; }
    }
}