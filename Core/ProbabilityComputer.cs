﻿using Core.Routing;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public interface IProbabilityComputer {

        /// <summary>
        /// Compute the probability maximum  of success given the valid routes and the hunters locations
        /// </summary>
        double ComputeHighestProbabilityOfSuccess(List<Route> routes, BountyHunterSchedule bountyHuntersSchedule, int countdown);
    }

    public class ProbabilityComputer: IProbabilityComputer
    {
        public double ComputeHighestProbabilityOfSuccess(List<Route> routes, BountyHunterSchedule bountyHuntersSchedule, int countdown)
        {
            if (routes.Count == 0)
                return 0.0;
            return routes.Max(route => 1 - ComputeLowestProbabilityOfFail(route.Steps.First, bountyHuntersSchedule, 0, countdown - route.TotalTravelTime, 0));
        }

        private double ComputeLowestProbabilityOfFail(LinkedListNode<RouteStep> currentStepNode, BountyHunterSchedule bountyHuntersSchedule, int dayOffset, int availableWaitingDays, int k)
        {
            if (currentStepNode == null)
                return 0.0;

            var currentPlanet = currentStepNode.Value.Planet;
            var currentDay = currentStepNode.Value.Day + dayOffset;

            var huntersAreHere = bountyHuntersSchedule.ArePresent(currentPlanet, currentDay);

            // Compute the probability without waiting a day
            var proba = ComputeLowestProbabilityOfFail(currentStepNode.Next, bountyHuntersSchedule, dayOffset, availableWaitingDays, huntersAreHere ? k + 1 : k) + (huntersAreHere ? Math.Pow(9, k) / Math.Pow(10, k + 1) : 0);

            // Compute the probability if we wait on the planet on the current day
            if (!huntersAreHere && availableWaitingDays > 0)
                proba = Math.Min(proba, ComputeLowestProbabilityOfFail(currentStepNode, bountyHuntersSchedule, dayOffset + 1, availableWaitingDays - 1, k));

            return proba;
        }
    }
}
