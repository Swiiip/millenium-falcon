using Core.Inputs;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class BountyHunterSchedule
    {
        private readonly Dictionary<string, HashSet<int>> _schedule;

        public BountyHunterSchedule(List<BountyHunterLocation> bountyHunterLocations)
        {
            _schedule = bountyHunterLocations
                .GroupBy(loc => loc.Planet, loc => loc.Day, (key, g) => new {Planet = key, Days = new HashSet<int>(g)})
                .ToDictionary(c => c.Planet, c => c.Days);;
        }

        public bool ArePresent(string planet, int day)
        {
            return _schedule.ContainsKey(planet) && _schedule[planet].Contains(day);
        }
    }
}
