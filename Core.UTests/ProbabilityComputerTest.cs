using Core.Inputs;
using Core.Routing;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Core.UTests
{
    public class ProbabilityComputerTest
    {
        private static IEnumerable<TestCaseData> ExampleTestCases
        {
            get
            {
                var testCaseStr = File.ReadAllText("./resources/example_test_cases.json");
                var testCasesSuccess = JsonHelper.TryDeserialize<List<ExampleTestCaseData>>(testCaseStr, out var result);

                return result.Select(c => new TestCaseData(c.expectedRoutes, c.bountyHunters, c.countdown, c.expectedProbability)).ToList();
            }
        }

        [TestCaseSource("ExampleTestCases")]
        public void TestExampleScenarios(List<Route> routes, List<BountyHunterLocation> hunterLocations, int countdown, double expectedProba) {
            var bountyHunterSchedule = new BountyHunterSchedule(hunterLocations);
            var probaComputer = new ProbabilityComputer();
            var proba = probaComputer.ComputeHighestProbabilityOfSuccess(routes, bountyHunterSchedule, countdown);
            Assert.AreEqual(proba, expectedProba);
        }
    }
}