using Core.Inputs;
using Core.Routing;
using System.Collections.Generic;

namespace Core.UTests {
    public class ExampleTestCaseData {
        public string origin;
        public string destination;
        public int autonomy;
        public int countdown;
        public List<Route> expectedRoutes;
        public List<BountyHunterLocation> bountyHunters;
        public double expectedProbability;
    }
}