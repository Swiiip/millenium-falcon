using Core;
using Core.Data;
using Core.Routing;
using Microsoft.Extensions.Logging;
using Moq;
using System.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;

namespace Core.UTests
{
    public class RoutePlannerTest
    {
        [SetUp]
        public void Setup()
        {
        }

        private static IEnumerable<TestCaseData> ExampleTestCases
        {
            get
            {
                var testCaseStr = File.ReadAllText("./resources/example_test_cases.json");
                var testCasesSuccess = JsonHelper.TryDeserialize<List<ExampleTestCaseData>>(testCaseStr, out var result);

                return result.Select(c => new TestCaseData(c.origin, c.destination, c.autonomy, c.countdown, c.expectedRoutes)).ToList();
            }
        }

        [TestCaseSource("ExampleTestCases")]
        public void TestExampleRoutes(string origin, string destination, int autonomy, int countdown, List<Route> expectedRoutes)
        {
            var jsonInfoReader = new JsonTravelInfoReader("./resources/test_universe.json", Mock.Of<ILogger>());
            var universe = new UniverseGraph(jsonInfoReader);
            var routePlanner = new RoutePlanner(universe);

            var validRoutes = routePlanner.ComputeValidRoutes(origin, destination, autonomy, countdown);

            Assert.AreEqual(expectedRoutes.Count, validRoutes.Count);

            for (var i = 0; i < validRoutes.Count; ++i) {
                Assert.AreEqual(expectedRoutes[i].Steps, validRoutes[i].Steps);
                Assert.AreEqual(expectedRoutes[i].TotalTravelTime, validRoutes[i].TotalTravelTime);
            }
        }
    }
}