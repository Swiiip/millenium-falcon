﻿using Core;
using Core.Inputs;
using Core.Routing;
using Core.Data;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = new ConsoleLogger();

            try
            {
                var parseMainArgsSuccess = TryParseMainArguments(args, logger, out var milleniumConf, out var empireConf, out var milleniumConfDir);

                if (!parseMainArgsSuccess) {
                    return;
                }

                var dbPathRelativeToExecutable = Path.Combine(milleniumConfDir, milleniumConf.DbPath);
                var coreService = BuildCoreService(dbPathRelativeToExecutable, logger);

                var probability = coreService.GetSuccessProbability(milleniumConf, empireConf);
                Console.WriteLine(probability);
            }
            catch (FileNotFoundException exception)
            {
                logger.LogError($"Error opening input files ({exception.Message})");
            }
            catch (Microsoft.Data.Sqlite.SqliteException exception) {
                logger.LogError($"Error opening sqlite database ({exception.Message})");
            }
        }

        private static bool TryParseMainArguments(string[] args, ILogger logger, out MilleniumFalconInput outMilleniumConf, out EmpireInput outEmpireConf, out string milleniumConfDir) {
            outMilleniumConf = null;
            outEmpireConf = null;
            milleniumConfDir = null;

            if (args.Length != 2) {
                logger.LogError("Wrong number of arguments");
                logger.LogError("Usage: dotnet run <path_to_millenium_falcon_file> <path_to_empire_file>");
                return false;
            }

            milleniumConfDir = Path.GetDirectoryName(args[0]);

            var milleniumConfString = File.ReadAllText(args[0]);
            var milleniumConfParseSuccess = JsonHelper.TryDeserialize<MilleniumFalconInput>(milleniumConfString, out var milleniumConf);

            if (!milleniumConfParseSuccess) {
                logger.LogError("Error parsing Millenium input");
                return false;
            }

            var empireConfString = File.ReadAllText(args[1]);
            var empireConfParseSuccess = JsonHelper.TryDeserialize<EmpireInput>(empireConfString, out var empireConf);

            if (!empireConfParseSuccess) {
                logger.LogError("Error parsing Empire input");
                return false;
            }

            outMilleniumConf = milleniumConf;
            outEmpireConf = empireConf;

            return true;
        }

        private static CoreService BuildCoreService(string dbPath, ILogger logger) {
            var travelInfoReader = new SqlTravelInfoReader(new SqlContext(dbPath), logger);
            var universeGraph = new UniverseGraph(travelInfoReader);
            var routePlanner = new RoutePlanner(universeGraph);
            var probabilityComputer = new ProbabilityComputer();
            var coreService = new CoreService(routePlanner, probabilityComputer);

            return coreService;
        }
    }
}
