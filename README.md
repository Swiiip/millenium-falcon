Millenium Falcon Project
===

Requirements
---
-  .NET Core 3.1 SDK


CLI
---
Command Line Interface to compute the Millenium Falcon success probabilities.

Usage

```
cd ./CLI/
dotnet run ../Resources/millenium-falcon.json ../Resources/empire.json
```


Core
---
Library providing the common elements between the Web Application and the CLI.

Core.UTests
---
Unit Tests for the Core Library. Based on the examples provided for the project.

Usage

```
cd ./Core.UTests/
dotnet test
```

Resources
---
Common resources.

Web
---
Simple ASP .NET core web app with 3 pages:
- Index: upload a JSON file containing the data intercepted by the rebels
- 404 page
- Error page

Usage

```
cd ./Web/
dotnet run
```