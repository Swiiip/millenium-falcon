﻿(function() {

    const resultDiv = document.getElementById("result");
    const label = document.getElementById("empire-file-label");
    const form = document.getElementById("upload-form");
    const fileInput = document.getElementById("empire-file-input");
    const sendButton = document.getElementById("send-button");

    async function processForm(e) {
        if (e.preventDefault) e.preventDefault();

        resultDiv.innerText = "";

        const form = new FormData(document.getElementById("upload-form"));
        const response = await fetch("/api/uploadEmpireJson",{ method: "POST", body: form });
        const jsonResponse = await response.json();

        if (jsonResponse.status == "success") {
            resultDiv.classList.add("text-success");
            resultDiv.classList.remove("text-danger");
            resultDiv.innerText =  `Probability of success: ${jsonResponse.value}%`;
        } else {
            resultDiv.classList.add("text-danger");
            resultDiv.classList.remove("text-success");
            resultDiv.innerText = jsonResponse.message;
        }

        return false;
    }

    function updateFileLoaderContent(e) {
        resultDiv.innerText = "";
        const files = e.target.files;
        if (files.length > 0 && files[0].name) {
            label.innerText = files[0].name;
            label.classList.add("file-loaded");
            sendButton.disabled = false;
        } else {
            label.innerText = "Pick a file";
            label.classList.remove("file-loaded");
            sendButton.disabled = true;
        }
    }

    form.addEventListener("submit", processForm);
    fileInput.addEventListener("change", updateFileLoaderContent)
})();
