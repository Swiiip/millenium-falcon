using Core;
using Core.Inputs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Web.Controllers
{
    [Route("api")]
    [ApiController]
    public class UploadFileController : Controller
    {
        private readonly ILogger<UploadFileController> _logger;
        private readonly ICoreService _coreService;
        private readonly MilleniumFalconInput _milleniumFalconInput;

        public UploadFileController(ILogger<UploadFileController> logger, ICoreService coreService, MilleniumFalconInput milleniumFalconInput)
        {
            _logger = logger;
            _coreService = coreService;
            _milleniumFalconInput = milleniumFalconInput;
        }

        [HttpPost("uploadEmpireJson")]
        public async Task<IActionResult> Index(IFormFile file)
        {
            if (file == null || file.Length == 0 || file.Length > 4000)
            {
                var message = file == null ? "File is null" : (file.Length == 0 ? "File is empty" : "File size limit exceeded (4Kb)");
                return Json(new { Status = "error", Message = message });
            }

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                var empireInputString = await reader.ReadToEndAsync();
                var parseSuccess = JsonHelper.TryDeserialize<EmpireInput>(empireInputString, out var empireInput);

                if (!parseSuccess || empireInput.BountyHunterLocations == null)
                {
                    _logger.Log(LogLevel.Warning, $"Error parsing Empire input file: {empireInputString.Substring(0, Math.Min(1000, empireInputString.Length))}");
                    return Json(new { Status = "error", Message = "Could not parse input file" });
                }

                var proba = _coreService.GetSuccessProbability(_milleniumFalconInput, empireInput);

                return Json(new { Status = "success", Value = proba * 100 });
            }
        }
    }
}
