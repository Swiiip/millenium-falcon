using Core;
using Core.Inputs;
using Core.Routing;
using Core.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;


namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRazorPages();

            // Register ICoreService
            services.AddSingleton<ICoreService>(c => {
                ILoggerFactory loggerFactory = c.GetService<ILoggerFactory>();

                var sqlContext = new SqlContext(Configuration.GetValue<string>("DbPath"));
                var travelInfoReader = new SqlTravelInfoReader(sqlContext, loggerFactory.CreateLogger<SqlTravelInfoReader>());
                var universeGraph = new UniverseGraph(travelInfoReader);
                var routePlanner = new RoutePlanner(universeGraph);
                var probabilityComputer = new ProbabilityComputer();
                var coreService = new CoreService(routePlanner, probabilityComputer);

                return coreService;
            });

            // Register static MilleniumFalconInput
            services.AddSingleton<MilleniumFalconInput>(c => {
                var milleniumConfString = System.IO.File.ReadAllText(Configuration.GetValue<string>("MilleniumFalconInputPath"));
                var milleniumConfParseSucces = JsonHelper.TryDeserialize<MilleniumFalconInput>(milleniumConfString, out var milleniumConf);

                if (!milleniumConfParseSucces)
                    throw new Exception("Could not parse Millenium Falcon input file");

                return milleniumConf;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseExceptionHandler("/Error");
            app.UseStatusCodePagesWithReExecute("/404");
            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
        }
    }
}
